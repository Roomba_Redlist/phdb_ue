// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class PHDBTarget : TargetRules
{
	public PHDBTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("PHDB");
		ExtraModuleNames.Add("PHDBLoadingScreen");
	}
}
