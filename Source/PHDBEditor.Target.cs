// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class PHDBEditorTarget : TargetRules
{
	public PHDBEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("PHDB");
		ExtraModuleNames.Add("PHDBLoadingScreen");
	}
}
