// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PHDB : ModuleRules
{
	public PHDB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new string[] { 
				"Core", 
				"CoreUObject",
				"Engine", 
				"InputCore", 
				"HeadMountedDisplay",
				"GameplayAbilities", 
				"GameplayTags", 
				"GameplayTasks" 
			});
		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"PHDBLoadingScreen",
				"Slate",
				"SlateCore",
				"InputCore",
				"MoviePlayer",
				"GameplayAbilities",
				"GameplayTags",
				"GameplayTasks"
			}
		);
	}
}
