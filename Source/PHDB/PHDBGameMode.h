// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PHDBGameMode.generated.h"

UCLASS(minimalapi)
class APHDBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APHDBGameMode();
};



