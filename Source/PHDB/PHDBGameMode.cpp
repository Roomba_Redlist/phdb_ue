// Copyright Epic Games, Inc. All Rights Reserved.

#include "PHDBGameMode.h"
#include "PHDBCharacter.h"
#include "UObject/ConstructorHelpers.h"

APHDBGameMode::APHDBGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
