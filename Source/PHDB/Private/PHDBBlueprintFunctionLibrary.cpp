// ProjectPHDB


#include "PHDBBlueprintFunctionLibrary.h"
#include "PHDBLoadingScreen.h"

UPHDBBlueprintFunctionLibrary::UPHDBBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}



void UPHDBBlueprintFunctionLibrary::PlayLoadingScreen(bool bPlayUntilStopped, float PlayTime)
{
	IPHDBLoadingScreenModule& LoadingScreenModule = IPHDBLoadingScreenModule::Get();
	LoadingScreenModule.StartInGameLoadingScreen(bPlayUntilStopped, PlayTime);
}

void UPHDBBlueprintFunctionLibrary::StopLoadingScreen()
{
	IPHDBLoadingScreenModule& LoadingScreenModule = IPHDBLoadingScreenModule::Get();
	LoadingScreenModule.StopInGameLoadingScreen();
}

