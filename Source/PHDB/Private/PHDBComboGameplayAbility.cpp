
#include "PHDBComboGameplayAbility.h"
#include "AbilitySystemComponent.h"

void UPHDBComboGameplayAbility::AddGameplayTags(const FGameplayTagContainer GameplayTags)
{
	UAbilitySystemComponent* Comp = GetAbilitySystemComponentFromActorInfo();

	Comp->AddLooseGameplayTags(GameplayTags);
}

void UPHDBComboGameplayAbility::RemoveGameplayTags(const FGameplayTagContainer GameplayTags)
{
	UAbilitySystemComponent* Comp = GetAbilitySystemComponentFromActorInfo();

	Comp->RemoveLooseGameplayTags(GameplayTags);
}